import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsabtenamComponent } from './formsabtenam/formsabtenam.component';

import {AppRoutingModule} from "./app.routing";
import {FormsModule} from "@angular/forms";
import { ListComponent } from './list/list.component';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TableModule} from "primeng/table";
import {HttpClientModule} from "@angular/common/http";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from "@angular/material/core";
import {MatInputModule} from "@angular/material/input";
import {ChatModule} from "./chat/chat.module";


@NgModule({
  declarations: [
    AppComponent,
    FormsabtenamComponent,
    ListComponent,
    MenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    TableModule,
    HttpClientModule,
    ButtonModule,
    InputTextModule,
    MatInputModule,
    ChatModule
  ],
  providers: [
      {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
    ],

  bootstrap: [AppComponent]
})
export class AppModule { }
