import { RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsabtenamComponent} from "./formsabtenam/formsabtenam.component";
import {ListComponent} from "./list/list.component";
import {MenuComponent} from "./menu/menu.component";
import {ChatoneComponent} from "./chat/chatone/chatone.component";
export { CommonModule } from '@angular/common';
export {BrowserModule} from '@angular/platform-browser';


const route: Routes = [
  {path: 'formsabtenam', component: FormsabtenamComponent},
 {path: '', component: ListComponent},
 {path:'chat',component:ChatoneComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(route)],
  exports: [RouterModule],
  declarations: [],

})
export class AppRoutingModule {
  ChatoneComponent;
}
