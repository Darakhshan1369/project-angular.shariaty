import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsabtenamComponent } from './formsabtenam.component';

describe('FormsabtenamComponent', () => {
  let component: FormsabtenamComponent;
  let fixture: ComponentFixture<FormsabtenamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsabtenamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsabtenamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
