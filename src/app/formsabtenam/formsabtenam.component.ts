import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Moshakhasat} from "./moshakhasat";
import {Router} from "@angular/router";
import {RestService} from "../rest.service";
import {TranslationService} from "../translation.service";


@Component({
  selector: 'app-formsabtenam',
  templateUrl: './formsabtenam.component.html',
  styleUrls: ['./formsabtenam.component.css']
})
export class FormsabtenamComponent implements OnInit {
  list= [];
  moshakhasat: Moshakhasat;
  constructor(private route: Router, private rest: RestService,private translat: TranslationService) {}



  ngOnInit(): void {
    this.moshakhasat={name:'',salary:null,age:null,id:null }
  }

  navigateToMenuComponent() {

    this.rest.save(this.moshakhasat).subscribe(response => {
      console.log('server response', response);
    });

    this.route.navigateByUrl('/');
  }

  }









