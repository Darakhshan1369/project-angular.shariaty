import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatthreeComponent } from './chatthree.component';

describe('ChatthreeComponent', () => {
  let component: ChatthreeComponent;
  let fixture: ComponentFixture<ChatthreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatthreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatthreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
