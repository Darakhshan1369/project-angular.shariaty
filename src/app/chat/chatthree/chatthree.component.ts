import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";

@Component({
  selector: 'app-chatthree',
  templateUrl: './chatthree.component.html',
  styleUrls: ['./chatthree.component.css']
})
export class ChatthreeComponent implements OnInit {

  constructor() { }

  message= '';

  @Input()
  messub1: Subject<any>;

  @Output()
  send1: EventEmitter<any> = new EventEmitter();


  ngOnInit(): void {

  }

  sendMessage1(){
    this.send1.emit(this.message);
    this.message= '';
  }



}
