import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatoneComponent } from './chatone.component';

describe('ChatoneComponent', () => {
  let component: ChatoneComponent;
  let fixture: ComponentFixture<ChatoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
