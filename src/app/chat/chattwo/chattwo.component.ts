import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from "rxjs";

@Component({
  selector: 'app-chattwo',
  templateUrl: './chattwo.component.html',
  styleUrls: ['./chattwo.component.css']
})
export class ChattwoComponent implements OnInit {

  constructor() {
  }

  @Input()
  messSub2: Subject<any>;

   message='';

  @Output()
  send2: EventEmitter<any> = new EventEmitter<any>();


  ngOnInit(): void {
  }


  sendMessage2() {
    this.send2.emit(this.message);
    this.message= '';
  }
}
