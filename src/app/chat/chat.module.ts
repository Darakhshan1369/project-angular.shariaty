import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatoneComponent } from './chatone/chatone.component';
import { ChattwoComponent } from './chattwo/chattwo.component';
import { ChatthreeComponent } from './chatthree/chatthree.component';
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [ChatoneComponent, ChattwoComponent, ChatthreeComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  bootstrap: [ChatoneComponent]
})
export class ChatModule {
  ChatoneComponent;
}
