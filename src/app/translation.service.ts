import { Injectable } from '@angular/core';
import {Moshakhasat} from "./formsabtenam/moshakhasat";

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  list= [];
  moshakhasat: Moshakhasat;
  constructor() { }

  send(moshakhasat){
    localStorage.setItem('moshakhasat', JSON.stringify(moshakhasat));
  }

  FetchMoshakhasat(): Moshakhasat {
    return JSON.parse(localStorage.getItem('moshakhasat'));
  }



}
