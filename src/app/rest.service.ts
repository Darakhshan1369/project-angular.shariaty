import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Moshakhasat} from "./formsabtenam/moshakhasat";


@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient ) { }

  getList(): Observable<any>{
   return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }

  save(moshakhasat: Moshakhasat): Observable<any> {
    return this.http.post('http://dummy.restapiexample.com/api/v1/create', moshakhasat);
  }

  updateperson(moshakhasat: Moshakhasat): Observable<any> {
    return this.http.put('http://dummy.restapiexample.com/api/v1/update/' + moshakhasat, moshakhasat);
  }

  delete(id: number): Observable<any> {
    return this.http.delete('http://dummy.restapiexample.com/api/v1/delete/' + id);
  }

}
