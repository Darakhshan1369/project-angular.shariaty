import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private rout: Router,private router: Router) {}


  ngOnInit(): void {
  }

  navigateToVasetComponent() {

    this.rout.navigateByUrl('/');

  }


  navegatToChat(){
    this.router.navigateByUrl('/chat');
  }

}
