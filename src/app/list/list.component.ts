import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {RestService} from "../rest.service";
import {Moshakhasat} from "../formsabtenam/moshakhasat";
import {TranslationService} from "../translation.service";
import {Subject} from "rxjs";


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


  constructor(private  router: Router, private rest: RestService) {
  }
  list=[];


  ngOnInit(): void {
    this.getlist();
  }

   navigateToFormsabtenamComponent(){

      this.router.navigateByUrl('/formsabtenam');
  }


  delete(id: number) {
    this.rest.delete(id).subscribe(res => {
      console.log('deleted', res);
      this.getlist();
    });

  }

   getlist() {
      this.rest.getList().subscribe(res => {
        console.log('list', res);
        this.list = res.data;
      });
    }

}

